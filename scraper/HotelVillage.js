const
  BrowserService = require('../services/BrowserService');

class HotelVillage {

  get _baseURL() {
    return 'https://book.omnibees.com/hotelresults';
  }

  set _browser(browser) {
    this.__browser = browser;
  }

  get _browser() {
    return this.__browser;
  }

  _mountURL({checkin, checkout}) {
    const
      checkinURLFormat = `${checkin.day}${checkin.month}${checkin.year}`,
      checkoutURLFormat = `${checkout.day}${checkout.month}${checkout.year}`;
    return `${this._baseURL}?CheckIn=${checkinURLFormat}&CheckOut=${checkoutURLFormat}&Code=AMIGODODANIEL&NRooms=1&_askSI=d34b1c89-78d2-45f3-81ac-4af2c3edb220&ad=2&ag=-&c=2983&ch=0&diff=false&group_code=&lang=pt-BR&loyality_card=&utm_source=asksuite&q=5462#show-more-hotel-button`;
  }

  async _getPage(url) {
    this._browser = await BrowserService.getBrowser()
    const
      page = await this._browser.newPage();
    await page.goto(url);
    return page;
  }

  async _closeBrowser() {
    await BrowserService.closeBrowser(this._browser);
    this._browser = null;
  }

  async scraper({checkin, checkout}) {
    try {
      const
        url = this._mountURL({checkin, checkout}),
        page = await this._getPage(url),
        rooms = await page.evaluate(() => {
          const
            roomsDiv = [...document.getElementsByClassName('roomrate')];
          return roomsDiv
            .filter(el => !el.classList.contains('d-none'))
            .map(el => {
              const
                name = el.querySelector('div.flex-view-step2 div.desciption span p.hotel_name').innerText,
                description = el.querySelector("div.flex-view-step2 div.desciption p.description").innerText.replace(' (ver mais)', ""),
                image = el.querySelector("div.flex-view-step2 div.t-tip__next div.image img.image-step2").src,
                price = [...el.getElementsByClassName('rate_plan')].find(p => p.dataset.rateName === "Tarifa não reembolsável").querySelector('div.right-part-of-rate div.price-and-button div.price-step2 p.price-total').innerText;
              return {
                name,
                description,
                price,
                image,
              }
            });
        });
      await this._closeBrowser();
      return rooms;
    } catch (e) {
      throw e;
    }
  }

}

module.exports = HotelVillage;