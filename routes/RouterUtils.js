const
  HTTPStatus = require("http-status-codes");

function sendResponse({res, success, error, status}) {
  if (!res || (!success && !error)) throw new Error('arguments required');
  if (error) return res.status(status || HTTPStatus.BAD_GATEWAY).send(error);
  return res.status(HTTPStatus.OK).send(success);
}

module.exports = {
  sendResponse,
};