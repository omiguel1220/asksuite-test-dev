const
  express = require('express'),
  router = express.Router(),
  routerUtils = require('./RouterUtils'),
  requestHandler = require('../handlers/Request'),
  HotelVillage = require('../scraper/HotelVillage');

router.get('/', (req, res) => {
  res.send('Hello Asksuite World!');
});

router.post('/search', async (req, res) => {
  try {
    const
      {body} = req;
    if (!body.checkin || !body.checkout) return routerUtils.sendResponse({
      res,
      error: 'checkinCheckoutRequired',
    });
    const
      trDate = requestHandler.transformDate(body.checkin, body.checkout);
    if (trDate.error) return routerUtils.sendResponse({
      res,
      error: trDate.error
    });
    const
      scrapper = new HotelVillage(),
      rooms = await scrapper.scraper(trDate.success);
    return routerUtils.sendResponse({res, success: rooms});
  } catch (e) {
    console.error(e);
  }
});

module.exports = router;