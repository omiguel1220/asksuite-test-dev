const
  path = require('path');
require('dotenv').config({path: path.resolve('.env.example')});
const express = require('express');
const router = require('./routes/router.js');

const app = express();
app.use(express.json());

const port = process.env.PORT;

app.use('/', router);
app.listen(port || 8080, () => {
    console.log(`Listening on port ${port}`);
});
