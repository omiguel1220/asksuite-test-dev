function transformDate(checkin, checkout) {
  const
    [
      yearCheckin,
      monthCheckin,
      dayCheckin,
    ] = checkin.split('-'),
    dateCheckin = new Date(yearCheckin, monthCheckin - 1, dayCheckin),
    currentDateBase = new Date(),
    currentDate = new Date(currentDateBase.getFullYear(), currentDateBase.getMonth(), currentDateBase.getDate());
  if (dateCheckin.getTime() < currentDate.getTime()) return {error: 'checkinMustBeGreatThanToday'};
  const
    [
      yearCheckout,
      monthCheckout,
      dayCheckout,
    ] = checkout.split('-'),
    dateCheckout = new Date(yearCheckout, monthCheckout - 1, dayCheckout);
  if (dateCheckout.getTime() < dateCheckin.getTime()) return {error: 'checkoutMustBeLessThanOrEqualCheckin'};
  return {
    success: {
      checkin: {
        year: yearCheckin,
        month: monthCheckin,
        day: dayCheckin,
      },
      checkout: {
        year: yearCheckout,
        month: monthCheckout,
        day: dayCheckout,
      }
    }
  };
}

module.exports = {
  transformDate,
};