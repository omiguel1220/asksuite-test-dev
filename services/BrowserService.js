const puppeteer = require('puppeteer');

class BrowserService {

    static getBrowser(options = {}) {
        return puppeteer.launch(options);
    }

    static closeBrowser(browser) {
        if (!browser) {
            return;
        }
        return browser.close();
    }
}

module.exports = BrowserService;
