const
  chai = require('chai'),
  expect = chai.expect,
  chaiHTTP = require('chai-http');
chai.use(chaiHTTP);

let
  baseURL;

before(() => {
  require('../server');
  baseURL = `http://localhost:${process.env.PORT}`;
})

describe('Test', () => {

  it('make first get', done => {
    chai.request(baseURL)
      .get('/')
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.text).to.equal('Hello Asksuite World!');
        done();
      });
  });

  it('search hotels - checkout not send', done => {
    chai.request(baseURL)
      .post('/search')
      .send({
        checkin: "2021-07-01",
      })
      .end((err, res) => {
        expect(res.status).to.equal(502);
        expect(res.error).to.be.instanceof(Object);
        expect(res.error).to.have.all.keys("status", "text", "method", "path");
        expect(res.error.text).to.equal("checkinCheckoutRequired");
        done();
      });
  });

  it('search hotels - checkin not send', done => {
    chai.request(baseURL)
      .post('/search')
      .send({
        checkout: "2021-07-03"
      })
      .end((err, res) => {
        expect(res.status).to.equal(502);
        expect(res.error).to.be.instanceof(Object);
        expect(res.error).to.have.all.keys("status", "text", "method", "path");
        expect(res.error.text).to.equal("checkinCheckoutRequired");
        done();
      });
  });

  it('search hotels - invalid checkin date', done => {
    chai.request(baseURL)
      .post('/search')
      .send({
        checkin: "2021-07-01",
        checkout: "2021-07-03"
      })
      .end((err, res) => {
        expect(res.status).to.equal(502);
        expect(res.error).to.be.instanceof(Object);
        expect(res.error).to.have.all.keys("status", "text", "method", "path");
        expect(res.error.text).to.equal("checkinMustBeGreatThanToday");
        done();
      });
  });

  it('search hotels - invalid checkout date', done => {
    const
      currentDate = new Date(),
      year = currentDate.getFullYear(),
      month = currentDate.getMonth() + 1,
      day = currentDate.getDate();
    chai.request(baseURL)
      .post('/search')
      .send({
        checkin: `${year}-${month > 9 ? month : `0${month}`}-${day}`,
        checkout: "2021-07-03"
      })
      .end((err, res) => {
        expect(res.status).to.equal(502);
        expect(res.error).to.be.instanceof(Object);
        expect(res.error).to.have.all.keys("status", "text", "method", "path");
        expect(res.error.text).to.equal("checkoutMustBeLessThanOrEqualCheckin");
        done();
      });
  });

  it('search hotels - cannot find rooms', done => {
    const
      currentDate = new Date(),
      year = currentDate.getFullYear(),
      month = currentDate.getMonth() + 1,
      day = currentDate.getDate();
    chai.request(baseURL)
      .post('/search')
      .send({
        checkin: `${year}-${month > 9 ? month : `0${month}`}-${day}`,
        checkout: `${year}-${month > 9 ? month : `0${month}`}-${day + 5}`,
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body).to.be.instanceof(Array);
        expect(res.body.length).to.equal(0);
        done();
      });
  }).timeout(15000);

  it('search hotels - ok', done => {
    const
      baseDate = new Date(2021, 8, 23),
      year = baseDate.getFullYear(),
      month = baseDate.getMonth() + 1,
      day = baseDate.getDate();
    chai.request(baseURL)
      .post('/search')
      .send({
        checkin: `${year}-${month > 9 ? month : `0${month}`}-${day}`,
        checkout: `${year}-${month > 9 ? month : `0${month}`}-${day + 5}`,
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body).to.be.instanceof(Array);
        res.body.forEach(room => {
          expect(room).to.be.instanceof(Object);
          expect(room).to.have.all.keys("name", "description", "price", "image");
          expect(room.name).to.be.a('string');
          expect(room.description).to.be.a('string');
          expect(room.price).to.be.a('string');
          expect(room.image).to.be.a('string');
        });
        done();
      });
  }).timeout(15000);

});